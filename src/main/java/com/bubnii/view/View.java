package com.bubnii.view;

import com.bubnii.controller.Controller;
import com.bubnii.model.User;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public class View {

    private static Logger logger = LogManager.getLogger(User.class);
    private static Controller controller = new Controller();
    private Map<String, String> menu;
    private Map<String, Printable> methodsMenu;
    private Scanner input;
    private User user = new User(23, "Serhii", "Bubnii");

    public View() {
        input = new Scanner(System.in);
        menu = new LinkedHashMap<>();
        menu.put("1", "  1 - Show the use of the invoke method ");
        menu.put("2", "  2 - Show the class fields that have been added to this annotation");
        menu.put("3", "  3 - Show the class methods that have been added to this annotation.");
        menu.put("4", "  3 - Show the  Invoke myMethod(String a, int ... args) and myMethod(String... args).");
        menu.put("Q", "  Q - exit");
        methodsMenu = new LinkedHashMap<>();
        methodsMenu.put("1", this::showInvoke);
        methodsMenu.put("2", this::showFields);
        methodsMenu.put("3", this::showMethod);
        methodsMenu.put("4", this::showInvokeMyMethod);
    }

    private void showInvoke() {
        controller.printInvokeMethod();
    }

    private void showFields() {
        controller.searchFieldAnnotated(user, user.getClass());
    }

    private void showMethod() {
        controller.searchMethodAnnotated(user.getClass());
    }

    private void showInvokeMyMethod() {
        controller.invokeMyMethod(user);
    }

    public void show() {
        String keyMenu;
        do {
            print("-----------------------------------------------------------------");
            outputMenu();
            print("Please, select menu point.");
            keyMenu = input.nextLine().toUpperCase();
            try {
                methodsMenu.get(keyMenu).print();
            } catch (Exception e) {
                e.printStackTrace();
            }
        } while (!keyMenu.equals("Q"));
    }

    private void outputMenu() {
        print("MENU:");
        for (String str : menu.values()) {
            print(str);
        }
    }

    public void print(Object object) {
        logger.info(object);
    }
}
