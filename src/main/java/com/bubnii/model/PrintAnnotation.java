package com.bubnii.model;

import com.bubnii.annotation.MethodAnnotation;
import com.bubnii.annotation.MyAnnotation;
import com.bubnii.view.View;

import java.lang.annotation.Annotation;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Arrays;

public class PrintAnnotation {

    private View view = new View();

    public StringBuilder searchAnnotatedField(Object obj, Class clazz) {
        StringBuilder builder = new StringBuilder();
        Field[] fields = clazz.getDeclaredFields();
        searchAnnotation(obj, builder, fields);
        view.print(builder);
        return builder;
    }

    private void searchAnnotation(Object obj, StringBuilder builder, Field[] fields) {
        for (Field field : fields) {
            Annotation[] annotations = field.getDeclaredAnnotations();
            for (Annotation annotation : annotations) {
                if (annotation.annotationType().equals(MyAnnotation.class)) {
                    try {
                        builder.append("@").append(annotation.annotationType().getSimpleName()).append(" (")
                                .append(field.getName()).append(" = ").append(field.get(obj)).append(")\n");
                    } catch (IllegalAccessException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }

    public StringBuilder searchhAnnotatedMethod(Class clazz) {
        StringBuilder builder = new StringBuilder();
        Method[] methods = clazz.getMethods();
        for (Method method : methods) {
            Annotation[] annotations = method.getDeclaredAnnotations();
            for (Annotation annotation : annotations) {
                if (annotation.annotationType().equals(MethodAnnotation.class)) {
                    builder.append("@").append(annotation.annotationType().getSimpleName()).append(" (")
                            .append(method.getName()).append(Arrays.toString(method.getParameterTypes())).append(")\n");
                }
            }
        }
        view.print(builder);
        return builder;
    }

    public void invokeMethod() {
        Class clazz = User.class;
        try {
            User user = getUser(clazz);
            invokeField(clazz, user);
            invokeParamerers(clazz, user);
            invokeParamerer(clazz, user);
        } catch (InstantiationException | IllegalAccessException | NoSuchMethodException
                | InvocationTargetException | NoSuchFieldException e) {
            e.printStackTrace();
        }
    }

    private void invokeParamerer(Class clazz, User user) throws NoSuchMethodException,
            IllegalAccessException, InvocationTargetException {
        Class[] methodParameter = {int.class};
        Method methodSquareAge = clazz.getDeclaredMethod("squareAge", methodParameter);
        methodSquareAge.setAccessible(true);
        methodSquareAge.invoke(user, 10);
    }

    private void invokeParamerers(Class clazz, User user) throws NoSuchMethodException,
            IllegalAccessException, InvocationTargetException {
        Class[] methodParameters = {String.class, String.class, int.class};
        Method methodPrintUser = clazz.getDeclaredMethod("printUser", methodParameters);
        methodPrintUser.setAccessible(true);
        methodPrintUser.invoke(user, "Ivan", "Ivanov", 50);
    }

    private void invokeField(Class clazz, User user) throws NoSuchFieldException,
            NoSuchMethodException, IllegalAccessException, InvocationTargetException {
        Field field = clazz.getDeclaredField("lastName");
        field.setAccessible(true);
        Method methodGetLastName = clazz.getDeclaredMethod("getLastName");
        field.set(user, "Bubnii");
        view.print(methodGetLastName.invoke(user));
    }

    private User getUser(Class clazz) throws NoSuchMethodException, InstantiationException,
            IllegalAccessException, InvocationTargetException {
        Class[] constructorParameters = {int.class, String.class, String.class};
        Constructor constructor = clazz.getDeclaredConstructor(constructorParameters);
        constructor.setAccessible(true);
        User user = (User) constructor.newInstance(20, "Serhii", "Bubniy");
        user.squareAge(23);
        Method methodGetName = clazz.getDeclaredMethod("getName");
        methodGetName.setAccessible(true);
        view.print(methodGetName.invoke(user));
        return user;
    }

    public boolean invokeTwoMyMethods(User obj) {
        try {
            Method myMethodFirst = User.class.getDeclaredMethod("myMethod", String.class, int[].class);
            Method myMethodSecond = User.class.getDeclaredMethod("myMethod", String[].class);
            if (((int) myMethodFirst.invoke(obj, "abcs", new int[]{100, 200, 400, 500}))
                    >= 1000 && (boolean) myMethodSecond.invoke(obj, new Object[]{
                    new String[]{"abcd", "sdf", "fgth", "btth"}})) {
                return true;
            }
        } catch (NoSuchMethodException | IllegalAccessException
                | InvocationTargetException e) {
            e.printStackTrace();
        }
        return false;
    }
}

