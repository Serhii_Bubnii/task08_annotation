package com.bubnii.model;

import com.bubnii.annotation.MethodAnnotation;
import com.bubnii.annotation.MyAnnotation;
import com.bubnii.view.View;

import java.util.stream.IntStream;


public class User {

    private static View view = new View();
    @MyAnnotation
    int age;
    @MyAnnotation
    protected String name;
    private String lastName;

    public User(int age, String name, String lastName) {
        this.age = age;
        this.name = name;
        this.lastName = lastName;
    }

    @MethodAnnotation
    public int getAge() {
        return age;
    }

    protected String getName() {
        return name;
    }

    String getLastName() {
        return lastName;
    }

    public void setAge(int age) {
        this.age = age;
    }

    private void setName(String name) {
        this.name = name;
    }

    @MethodAnnotation
    private void printUser(String name, String lastName, int age) {
        view.print("Name = " + name + ", last name = " + lastName + ", age = " + age);
    }

    @MethodAnnotation
    public void squareAge(int age) {
        view.print(age * age);
    }

    public String myMethod(String a, int... args) {
        return a + " = " + IntStream.of(args).sum();
    }

    public boolean myMethod(String... args) {
        return args.length == args[0].length();
    }
}
