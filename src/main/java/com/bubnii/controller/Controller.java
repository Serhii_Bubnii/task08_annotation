package com.bubnii.controller;

import com.bubnii.model.PrintAnnotation;
import com.bubnii.model.User;

public class Controller {

    private PrintAnnotation printAnnotation;

    public Controller() {
        this.printAnnotation = new PrintAnnotation();
    }

    public void searchFieldAnnotated(Object obj, Class clazz) {
        printAnnotation.searchAnnotatedField(obj, clazz);
    }

    public void searchMethodAnnotated(Class clazz) {
        printAnnotation.searchhAnnotatedMethod(clazz);
    }

    public void printInvokeMethod(){
        printAnnotation.invokeMethod();
    }

    public void invokeMyMethod(User user){
        printAnnotation.invokeTwoMyMethods(user);
    }
}
